# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sleepiq/version'

Gem::Specification.new do |spec|
  spec.name          = 'sleepiq'
  spec.version       = SleepIQ::VERSION
  spec.authors       = ['esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'SleepIQ Client Library'
  spec.description   = 'Used to connect to the SleepIQ Internal API'
  spec.homepage      = 'https://bitbucket.org/Esity/sleepiq'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.5.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/Esity/sleepiq'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/Esity/sleepiq/src/master/CHANGELOG.md'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-rake'
  spec.add_development_dependency 'simplecov'

  spec.add_dependency 'faraday'
  spec.add_dependency 'faraday_middleware'
  spec.add_dependency 'thor'
end
