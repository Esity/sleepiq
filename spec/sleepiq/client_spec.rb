require 'spec_helper'

RSpec.describe SleepIQ::Client do
  it 'is a class' do
    expect(SleepIQ::Client).to be_a Class
  end

  it 'can init with values passed' do
    expect do
      SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    end.not_to raise_error
  end

  it 'can init without values passed' do
    expect { SleepIQ::Client.new }.not_to raise_error
  end

  it 'can login' do
    connection = SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    expect(connection.sessid).not_to be nil
    expect(connection.awsalb).not_to be nil
    expect(connection.key).not_to be nil
  end

  it 'can get family_status' do
    connection = SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    expect { connection.family_status }.not_to raise_error
    family_status = connection.family_status
    expect(family_status).to be_a Hash
  end
end
