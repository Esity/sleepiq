require 'spec_helper'

RSpec.describe SleepIQ::Family do
  it 'is a class' do
    expect(SleepIQ::Family).to be_a Module
  end

  it 'can get family_status' do
    connection = SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    expect { connection.family_status }.not_to raise_error
    family_status = connection.family_status
    expect(family_status).to be_a Hash
  end
end
