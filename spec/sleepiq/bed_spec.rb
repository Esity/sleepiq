require 'spec_helper'

RSpec.describe SleepIQ::Bed do
  it 'is a module' do
    expect(SleepIQ::Bed).to be_a Module
  end

  it 'can get bed' do
    connection = SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    expect { connection.bed }.not_to raise_error
    expect(connection.bed).to be_a Hash
  end

  it 'can get status' do
    connection = SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    expect { connection.status }.not_to raise_error
  end

  it 'can get sleep number' do
    connection = SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    expect { connection.sleep_number 'L' }.not_to raise_error
    expect(connection.sleep_number('L')).to be_a Hash
  end

  it 'can set sleep number' do
    connection = SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    expect { connection.update_sleep_number(100, 'L') }.not_to raise_error
    expect { connection.update_sleep_number(100, 'R') }.not_to raise_error
  end

  it 'can get pause mode' do
    connection = SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    expect { connection.pause_mode }.not_to raise_error
  end

  it 'can get fav sleep number' do
    connection = SleepIQ::Client.new(username: ENV['sleepiq_username'], password: ENV['sleepiq_password'])
    expect { connection.sleep_number_fav 'L' }.not_to raise_error
  end
end
