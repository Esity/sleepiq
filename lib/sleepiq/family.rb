module SleepIQ
  module Family
    def family_status
      result = @conn.get { |req| req.url 'rest/bed/familyStatus' }
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end
  end
end
