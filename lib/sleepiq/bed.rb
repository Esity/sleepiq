module SleepIQ
  module Bed
    def bed
      result = @conn.get { |req| req.url 'rest/bed' }
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def status(bedid = @bedid)
      result = @conn.get { |req| req.url "rest/bed/#{bedid}/status" }
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def sleep_number(side, bedid = @bedid)
      result = @conn.get do |req|
        req.url "rest/bed/#{bedid}/sleepNumber"
        req.params = { side: side, _k: @key }
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def update_sleep_number(sleep_number, side, bedid = @bedid)
      result = @conn.put do |req|
        req.url "rest/bed/#{bedid}/sleepNumber"
        req.body = "{\"sleepNumber\":\"#{sleep_number}\",\"side\":\"#{side}\"}"
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def sleep_number_fav(side, bedid = @bedid)
      result = @conn.get do |req|
        req.url "rest/bed/#{bedid}/sleepNumberFavorite"
        req.params = { side: side, _k: @key }
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def update_sleep_number_fav
      result = @conn.put do |req|
        req.url "rest/bed/#{bedid}/sleepNumberFavorite"
        req.body = "{\"sleepNumber\":\"#{sleep_number}\",\"side\":\"#{side}\"}"
        req.params = { side: side, _k: @key }
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def pause_mode(bedid = @bedid)
      result = @conn.get { |req| req.url "rest/bed/#{bedid}/pauseMode" }
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def update_pause_mode(mode = 'off', bedid = @bedid)
      result = @conn.put do |req|
        req.url "rest/bed/#{bedid}/pauseMode"
        req.params = { mode: mode }
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end
  end
end
