module SleepIQ
  module Foundation
    def light(light = 1)
      result = @conn.get do |req|
        req.url "rest/bed/#{bedid}/foundation/outlet"
        req.params = { outletId: light, _k: @key }
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def set_light(light, setting = 0, bedid = @bedid)
      result = @conn.put do |req|
        req.url "rest/bed/#{bedid}/foundation/outlet"
        req.body = "\"outletId\":\"#{light}\",\"setting\":#{setting}}"
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def preset(preset, side, slow_speed = 0, bedid = @bedid)
      result @conn.put do |req|
        req.url "rest/bed/#{bedid}/foundation/preset"
        req.body = "{\"preset\":#{preset}, \"speed\":#{slow_speed}, \"side\":#{side}}"
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def stop_motion(side, bedid = @bedid)
      result @conn.put do |req|
        req.url "rest/bed/#{bedid}/foundation/motion"
        req.body = "{\"footMotion\":1, \"headMotion\":1, \"massageMotion\":1, \"side\":#{side}}"
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def foundation_status
      result = @conn.get { |req| req.url "rest/bed/#{bedid}/foundation/status" }
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def system
      result = @conn.get { |req| req.url "rest/bed/#{bedid}/foundation/system" }
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end
  end
end
