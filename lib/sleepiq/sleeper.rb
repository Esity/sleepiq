module SleepIQ
  module Sleeper
    def sleeper
      result = @conn.get { |req| req.url 'rest/sleeper' }
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def sleep_data(date, interval = 'D1')
      result = @conn.get do |req|
        req.url 'rest/sleepData'
        req.params = { date: date, interval: interval, _k: @key }
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def sleep_slice_data(date, interval = 'D1')
      result = @conn.get do |req|
        req.url 'rest/sleepSliceData'
        req.params = { date: date, interval: interval, _k: @key }
      end
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end
  end
end
