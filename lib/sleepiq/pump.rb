module SleepIQ
  module Pump
    def pump_status(bedid = @bedid)
      result = @conn.get { |req| req.url "rest/bed/#{bedid}/pump/status" }
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end

    def force_idle(bedid = @bedid)
      result = @conn.put { |req| req.url "rest/bed/#{bedid}/pump/forceIdle" }
      raise "status code was #{result.status}, #{result.body}" unless result.status == 200

      result.body
    end
  end
end
