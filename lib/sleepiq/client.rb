require 'faraday'
require 'faraday_middleware'

module SleepIQ
  class Client
    attr_accessor :key, :sessid, :awsalb, :bedid

    include SleepIQ::Family
    include SleepIQ::Bed
    include SleepIQ::Foundation
    include SleepIQ::Pump
    include SleepIQ::Sleeper

    def initialize(**opts) # rubocop:disable Metrics/AbcSize
      @username = opts[:username].nil? ? ENV['sleepiq_username'] : opts[:username]
      @password = opts[:password].nil? ? ENV['sleepiq_password'] : opts[:password]
      @key = opts[:key]
      @awsalb = opts[:awsalb]
      @sessid = opts[:sessid]
      @bedid = opts[:bedid]

      login if @key.nil? || @awsalb.nil? || @sessid.nil?

      @conn = Faraday.new(
        url:     'https://api.sleepiq.sleepnumber.com',
        headers: default_headers,
        params:  { _k: @key },
        request: { timeout: 10, open_timeout: 3, write_timeout: 10 }
      ) do |conn|
        conn.response :json
        conn.adapter :net_http
      end

      @bedid = family_status['beds'][0]['bedId'] if @bedid.nil?
    end

    def login(username = @username, password = @password) # rubocop:disable Metrics/AbcSize
      login_conn = Faraday.new(
        url:     'https://api.sleepiq.sleepnumber.com',
        ssl:     { verify: false },
        headers: { 'Content-Type': 'text/plain', 'Host': 'api.sleepiq.sleepnumber.com' }
      ) do |conn|
        conn.response :json
        conn.adapter :net_http
      end

      result = login_conn.put do |req|
        req.url '/rest/login'
        req.options.timeout = 10
        req.options.write_timeout = 10
        req.options.open_timeout = 3
        req.body = "{\"login\":\"#{username}\",\"password\":\"#{password}\"}"
      end

      raise "status code was #{result.status}" unless result.status == 200

      result.headers['set-cookie'].split(' ').each do |k, _v|
        @sessid = k.split('=')[1].delete_suffix(';') if k.include? 'JSESSIONID'
        @awsalb = k.split('=')[1].delete_suffix(';') if k.include? 'AWSALB'
      end
      @key = result.body['key']
    end

    def default_headers
      { 'Content-Type':    'text/plain',
        'Host':            'api.sleepiq.sleepnumber.com',
        'Accept':          '*/*',
        'Cache-Control':   'no-cache',
        'Accept-Encoding': 'gzip, deflate',
        'Connection':      'keep-alive',
        'Cookie':          "AWSALB=#{@awsalb}; JSESSIONID=#{@sessid}" }
    end
  end
end
