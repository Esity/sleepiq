# SleepIQ Changelog
## v0.3.0
* Removing Dalli dependency
* Changing ruby version to >= 2.5.0

## v0.2.2
Bug fixes

## V0.2.0
* Removed dalli(memcached logic)
* Updated rubocop.yml

## v0.1.1
* Adding foundation methods however these are untested
* Adding method for getting and setting favorite sleep number
* Adding additional RSpec test

##v0.1.0
initial commit